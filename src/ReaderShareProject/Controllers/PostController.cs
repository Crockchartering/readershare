﻿namespace GibixOnline.ReaderShareProject.Controllers {
	#region Using directives

	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.IO;
	using System.Linq;
	using System.Net;
	using System.Text;
	using System.Web;
	using System.Web.Mvc;
	using System.Web.Script.Serialization;

	using Dapper;

	using GibixOnline.ReaderShareProject.Models;
	using GibixOnline.ReaderShareProject.Support;

	using HtmlAgilityPack;

	#endregion

	/// <summary>
	/// Handles all of the logic for managing posts.
	/// </summary>
	public class PostController : BaseController {
		/// <summary>
		/// Loads the add post view.
		/// </summary>
		/// <returns>ActionResult to the default Add view.</returns>
		[HttpGet]
		public ActionResult Add() {
			if (this.ReaderUser == null) {
				return this.RedirectToAction("Login", "User");
			}

			return this.View();
		}

		/// <summary>
		/// Creates a new post.
		/// </summary>
		/// <param name="title">The title.</param>
		/// <param name="summary">The summary.</param>
		/// <param name="content">The content.</param>
		/// <param name="isPublic">The is public.</param>
		/// <returns>
		/// Redirects to the add view.
		/// </returns>
		[HttpPost]
		public ActionResult Create(string title, string summary, string content, bool? isPublic) {
			if (this.ReaderUser == null) {
				return this.RedirectToAction("Login", "User");
			}

			using (IDbConnection connection = Engine.GetConnection()) {
				connection.Open();
				connection.Execute(
					"INSERT INTO reader.posts (OwnerId, Public, Title, Summary, Content) VALUES (:OwnerId, :Public, :Title, :Summary, :Content)",
					new {
						OwnerId = this.ReaderUser.Id,
						Public = isPublic ?? false,
						Title = title,
						Summary = String.IsNullOrWhiteSpace(summary) ? null : summary,
						Content = String.IsNullOrWhiteSpace(content) ? null : content
					});
				connection.Close();
			}

			return this.RedirectToAction("Add");
		}

		/// <summary>
		/// Returns the default index view containing the number of requested posts.
		/// </summary>
		/// <param name="limit">The maximum number of posts to return.</param>
		/// <returns>The Index view with the requested posts.</returns>
		[HttpGet]
		public ActionResult Index(int? limit) {
			if (this.ReaderUser == null) {
				return this.RedirectToAction("Login", "User");
			}

			int max = 20;
			if (limit.HasValue) {
				max = Math.Max(1, Math.Min(100, limit.Value));
			}

			List<Post> posts = new List<Post>();
			using (IDbConnection connection = Engine.GetConnection()) {
				connection.Open();
				posts.AddRange(
					connection.Query<Post>(
						"SELECT Id, Published, LastUpdated, Public, Title, Summary, OriginalUri OriginalUriString, Content FROM reader.posts WHERE OwnerId = :OwnerId ORDER BY Published DESC LIMIT :Max;",
						new {
							OwnerId = this.ReaderUser.Id,
							MAX = max
						}));
				connection.Close();
			}

			return this.View(posts);
		}

		[HttpGet]
		public ActionResult Share(string title, string source, Uri url, bool? content = true) {
			Guid token;
			if (!Guid.TryParse(this.RouteData.Values["token"] as string, out token)) {
				return this.HttpNotFound();
			}

			if (String.IsNullOrWhiteSpace(title) || url == null || !url.IsAbsoluteUri) {
				return new HttpStatusCodeResult(400);
			}

			using (IDbConnection connection = Engine.GetConnection()) {
				connection.Open();

				int? userId = connection.Query<int?>("reader.get_user_id_from_key", new { post_key = token }, commandType: CommandType.StoredProcedure).SingleOrDefault();
				if (userId == null) {
					return this.HttpNotFound();
				}

				string htmlContent = null;
				if (content == true) {
					try {
						htmlContent = RequestReadabilityData(url);
					}
					catch (WebException wex) {
						this.ViewBag.ExceptionMessage = wex.Message;
					}
				}

				string postTitle = "{0} - {1}".FormatIC(source, title);
				if (!String.IsNullOrWhiteSpace(htmlContent)) {
					connection.Execute(
						"INSERT INTO reader.posts (OwnerId, Public, Title, OriginalUri, Content) VALUES (:OwnerId, :Public, :Title, :OriginalUri, :Content)",
						new {
							OwnerId = userId,
							Public = true,
							Title = postTitle,
							OriginalUri = url.AbsoluteUri,
							Content = htmlContent
						});
				} else {
					connection.Execute(
						"INSERT INTO reader.posts (OwnerId, Public, Title, OriginalUri) VALUES (:OwnerId, :Public, :Title, :OriginalUri)",
						new {
							OwnerId = userId,
							Public = true,
							Title = postTitle,
							OriginalUri = url.AbsoluteUri
						});
				}

				connection.Close();
			}

			return this.View("ReaderSaved");
		}

		/// <summary>
		/// Creates a new web request mimicking IE9 on Windows 7.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <returns>A new HttpWebRequest object with certain HTTP headers already set.</returns>
		static HttpWebRequest CreateWebRequest(string url) {
			HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
			webRequest.KeepAlive = false;
			webRequest.ServicePoint.Expect100Continue = false;
			webRequest.Accept = "text/html, application/xhtml+xml, */*";
			webRequest.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
			webRequest.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)";
			webRequest.Headers["Accept-Language"] = "en-US";
			return webRequest;
		}

		/// <summary>
		/// Requests the article html from Readability.
		/// </summary>
		/// <param name="articleUrl">The URL of the article to convert.</param>
		/// <returns>The article HTML if successful; <c>null</c> otherwise.</returns>
		static string RequestReadabilityData(Uri articleUrl) {
			CookieContainer cookies = new CookieContainer();
			byte[] postData = Encoding.UTF8.GetBytes(String.Concat("url=", HttpUtility.UrlEncode(articleUrl.AbsoluteUri)));

			// Request readability to shorten the URL first. This is the easy way to get the article id.
			HttpWebRequest webRequest = CreateWebRequest("http://www.readability.com/api/shortener/v1/urls");
			webRequest.Method = "POST";
			webRequest.ContentLength = postData.Length;
			webRequest.ContentType = "application/x-www-form-urlencoded";
			webRequest.CookieContainer = cookies;
			webRequest.KeepAlive = false;
			webRequest.ServicePoint.Expect100Continue = false;
			using (Stream stream = webRequest.GetRequestStream()) {
				stream.Write(postData, 0, postData.Length);
			}

			string jsonString;
			HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
			using (Stream stream = response.GetResponseStream()) {
				if (stream == null) {
					return null;
				}

				using (StreamReader reader = new StreamReader(stream, Encoding.UTF8)) {
					jsonString = reader.ReadToEnd();
				}
			}

			// Convert the response to a JSON object to confirm success and grab the article id.
			JavaScriptSerializer javascriptSerializer = new JavaScriptSerializer();
			dynamic createResponse = javascriptSerializer.DeserializeObject(jsonString);
			if (!createResponse["success"]) {
				return null;
			}
			
			string articleId = createResponse["meta"]["id"];

			// To sort of mimic a browser, hit the actual article URL first - so we look kind of normal.
			webRequest = CreateWebRequest("http://www.readability.com/articles/{0}".FormatIC(articleId));
			webRequest.CookieContainer = cookies;
			webRequest.GetResponse();

			// Now request the full article by appending ?readbar=1 to the request, this gets the article in a <section> tag.
			webRequest = CreateWebRequest("http://www.readability.com/articles/{0}?readbar=1".FormatIC(articleId));
			webRequest.CookieContainer = cookies;
			response = (HttpWebResponse)webRequest.GetResponse();

			string html;
			using (Stream stream = response.GetResponseStream()) {
				if (stream == null) {
					return null;
				}

				using (StreamReader reader = new StreamReader(stream, Encoding.UTF8)) {
					html = reader.ReadToEnd();
				}
			}

			// Load the response into an HTML document.
			HtmlDocument document = new HtmlDocument();
			document.LoadHtml(html);

			// Grab the article node, cutting out unneeded data.
			HtmlNode articleNode = document.DocumentNode.SelectSingleNode("//section[@id='rdb-article-content']/div");
			if (articleNode == null) {
				return null;
			}

			// If there is a <h3 class="post-name"> extract the name and convert it to a div with a font-size of 14.
			HtmlNode titleNode = articleNode.SelectSingleNode(".//h3[@class='post-name']");
			if (titleNode != null) {
				HtmlNode newTitleNode = document.CreateElement("div");
				newTitleNode.Attributes.Add("style", "font-size: 14pt;");
				newTitleNode.InnerHtml = titleNode.InnerText;
				titleNode.ParentNode.ReplaceChild(newTitleNode, titleNode);
			}

			// If there is a <div class="post-author"> extract the text and convert it to a div with a font size of 12 and a little padding.
			HtmlNode authorNode = articleNode.SelectSingleNode(".//div[@class='post-author']");
			if (authorNode != null) {
				HtmlNode newAuthorNode = document.CreateElement("div");
				newAuthorNode.Attributes.Add("style", "font-size: 12pt; padding-left: 1em; padding-top: .5em;");
				newAuthorNode.InnerHtml = authorNode.InnerText;
				authorNode.ParentNode.ReplaceChild(newAuthorNode, authorNode);
			}

			// Wrap the content of the article in a div to set a standard font size.
			HtmlNode wrapper = document.CreateElement("div");
			wrapper.Attributes.Add("style", "font-family: Helvetica, Verdana; font-size: 10pt;");
			wrapper.AppendChild(articleNode);

			// Remove &#13; and any outside whitespace.
			return wrapper.OuterHtml.Replace("&#13;", "").Trim();
		}
	}
}
