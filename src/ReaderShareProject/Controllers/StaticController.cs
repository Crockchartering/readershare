﻿namespace GibixOnline.ReaderShareProject.Controllers {
	#region Using directives

	using System.Web.Mvc;

	#endregion

	/// <summary>
	/// Default static controller for static pages.
	/// </summary>
	public class StaticController : Controller {
		/// <summary>
		/// Default index view.
		/// </summary>
		/// <returns>Returns the default view for this action.</returns>
		public ActionResult Index() { return this.View(); }
	}
}
