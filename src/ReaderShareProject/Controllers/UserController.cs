﻿namespace GibixOnline.ReaderShareProject.Controllers {
	#region Using directives

	using System;
	using System.Data;
	using System.Linq;
	using System.Web.Mvc;
	using System.Web.Security;

	using Dapper;

	using DotNetOpenAuth.Messaging;
	using DotNetOpenAuth.OpenId;
	using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
	using DotNetOpenAuth.OpenId.RelyingParty;

	using GibixOnline.ReaderShareProject.Support;

	#endregion

	/// <summary>
	/// Coordinates actions involving the user objects.
	/// </summary>
	public class UserController : BaseController {
		/// <summary>
		/// Creates a new user account.
		/// </summary>
		/// <param name="name">The user's preferred name.</param>
		/// <param name="email">The user's email address.</param>
		/// <param name="openId">The user's open id.</param>
		/// <returns>An action result.</returns>
		[HttpPost]
		public ActionResult Create(string name, string email, string openId) {
			if (String.IsNullOrWhiteSpace(name) || String.IsNullOrWhiteSpace(email)) {
				this.TempData["CreateError"] = "Missing required data.";
				return this.RedirectToAction("SignUp");
			}

			if (String.CompareOrdinal(this.Session["OpenId"] as string, openId) == 0) {
				using (IDbConnection connection = Engine.GetConnection()) {
					connection.Open();

					using (IDbTransaction transaction = connection.BeginTransaction()) {
						if (connection.Query<int?>("SELECT Id FROM reader.claimed_ids WHERE Identifier = :Identifier", new { Identifier = openId }, transaction).Any()) {
							transaction.Rollback();
							this.TempData["CreateError"] = "Claimed ID already exists.";
							return this.RedirectToAction("SignUp");
						}

						if (connection.Query<int?>("SELECT Id FROM reader.users WHERE UPPER(Email) = :Email", new { Email = email.ToUpperInvariant() }, transaction).Any()) {
							transaction.Rollback();
							this.TempData["CreateError"] = "Email already in use.";
							return this.RedirectToAction("SignUp");
						}

						int? userId = connection.Query<int?>(
							"INSERT INTO reader.users (Email, EmailMD5, Name, Token, PostingKey) VALUES (:Email, :EmailMD5, :Name, :Token, :PostingKey) RETURNING Id",
							new {
								Email = email,
								EmailMD5 = Utilities.MD5Hash(email.Trim().ToLowerInvariant()),
								Name = name,
								Token = Guid.NewGuid(),
								PostingKey = Guid.NewGuid()
							},
							transaction).SingleOrDefault();

						if (userId == null) {
							transaction.Rollback();
							this.TempData["CreateError"] = "Unknown error";
							return this.RedirectToAction("SignUp");
						}

						int? claimedId = connection.Query<int?>(
							"INSERT INTO reader.claimed_ids (Identifier, OwnerId) VALUES (:Identifier, :OwnerId) RETURNING Id",
							new {
								Identifier = openId,
								OwnerId = userId
							},
							transaction).SingleOrDefault();

						if (claimedId == null) {
							transaction.Rollback();
							this.TempData["CreateError"] = "Unknown error";
							return this.RedirectToAction("SignUp");
						}

						transaction.Commit();
					}
				}

				FormsAuthentication.SetAuthCookie(openId, true);
				return this.RedirectToAction("Home");
			}

			return this.RedirectToAction("SignUp");
		}

		[HttpPost]
		public ActionResult Generate(string type) {
			if (this.ReaderUser == null) {
				return this.RedirectToAction("Home");
			}

			if (String.CompareOrdinal(type, "feed") == 0 || String.CompareOrdinal(type, "posting") == 0) {
				using (IDbConnection connection = Engine.GetConnection()) {
					connection.Open();
					connection.Execute(
						"reader.update_user_key(:user_id, :key_type, :new_key)",
						new { user_id = this.ReaderUser.Id, key_type = type, new_key = Guid.NewGuid() },
						commandType: CommandType.StoredProcedure);
					connection.Close();
				}
			}

			return this.RedirectToAction("Home");
		}

		/// <summary>
		/// Returns the home view.
		/// </summary>
		/// <returns>The home view.</returns>
		[HttpGet]
		public ActionResult Home() {
			if (this.ReaderUser == null) {
				return this.RedirectToAction("SignUp");
			}

			using (IDbConnection connection = Engine.GetConnection()) {
				connection.Open();
				ViewBag.PostCount = connection.Query<Int64?>("SELECT COUNT(Id) FROM reader.posts WHERE OwnerId = :OwnerId", new { OwnerId = this.ReaderUser.Id }).SingleOrDefault();
				connection.Close();
			}

			return this.View(this.ReaderUser);
		}

		/// <summary>
		/// Attempts to log the user in via Open ID.
		/// </summary>
		/// <returns>The SignUp view if there the account doesn't exist, the Home view if successful, the login view otherwise.</returns>
		[HttpGet]
		public ActionResult Login() {
			OpenIdRelyingParty party = new OpenIdRelyingParty();
			IAuthenticationResponse response = party.GetResponse();

			if (response != null) {
				switch (response.Status) {
					case AuthenticationStatus.Authenticated:
						int? userId;
						string claimedId = response.ClaimedIdentifier;
						using (IDbConnection connection = Engine.GetConnection()) {
							connection.Open();
							userId = connection.Query<int?>("reader.login_user", new { claimed_id = claimedId }, commandType: CommandType.StoredProcedure).SingleOrDefault();
							connection.Close();
						}

						if (userId == null) {
							ClaimsResponse userData = response.GetExtension<ClaimsResponse>();
							this.TempData["Email"] = userData.Email;
							this.TempData["Name"] = userData.Nickname;
							this.Session["OpenId"] = claimedId;
							return this.RedirectToAction("SignUp");
						}

						FormsAuthentication.SetAuthCookie(claimedId, true);
						return this.RedirectToAction("Home");
				}
			}

			FormsAuthentication.SignOut();
			return this.View();
		}

		/// <summary>
		/// Attempts to log the user in via Open ID.
		/// </summary>
		/// <param name="openId">The open id URL.</param>
		/// <returns>The login view if there is an error or a redirect response to the Open ID provider.</returns>
		[HttpPost]
		public ActionResult Login(string openId) {
			if (String.IsNullOrWhiteSpace(openId) || !Identifier.IsValid(openId)) {
				this.ViewData["Error"] = "The specified login identifier is invalid.";
				return this.View();
			}

			OpenIdRelyingParty party = new OpenIdRelyingParty();
			IAuthenticationRequest request = party.CreateRequest(Identifier.Parse(openId));
			request.AddExtension(new ClaimsRequest { Email = DemandLevel.Require, Nickname = DemandLevel.Request });
			return request.RedirectingResponse.AsActionResult();
		}

		/// <summary>
		/// Logs the user out from forms authentication.
		/// </summary>
		/// <returns>Redirects to the static index view.</returns>
		[HttpGet]
		public ActionResult Logout() {
			FormsAuthentication.SignOut();
			return this.RedirectToAction("Index", "Static");
		}

		/// <summary>
		/// Returns the sign up page.
		/// </summary>
		/// <returns>The sign up page.</returns>
		[HttpGet]
		public ActionResult SignUp() {
			this.ViewData["Email"] = this.TempData["Email"];
			this.ViewData["Name"] = this.TempData["Name"];
			this.ViewData["OpenId"] = this.Session["OpenId"];
			this.ViewData["Error"] = this.TempData["Error"];
			return this.View();
		}
	}
}
