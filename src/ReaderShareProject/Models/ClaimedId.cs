﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GibixOnline.ReaderShareProject.Models {
	using System.ComponentModel.DataAnnotations;

	public class ClaimedId {
		public virtual User User { get; set; }
		[Key]
		[MaxLength(1024)]
		public string Identifier { get; set; }
	}
}