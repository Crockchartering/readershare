﻿namespace GibixOnline.ReaderShareProject.Models {
	#region Using directives

	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;

	#endregion

	/// <summary>
	/// Represents a user in the application.
	/// </summary>
	public class User {
		/// <summary>
		/// Holds the clamed ids for the user.
		/// </summary>
		ICollection<ClaimedId> clamedIds;

		/// <summary>
		/// Holds the friend parings for the user.
		/// </summary>
		ICollection<Friendship> friends;

		/// <summary>
		/// Holds the posts that are owned by the user.
		/// </summary>
		ICollection<Post> posts;

		/// <summary>
		/// Initializes a new instance of the <see cref="User"/> class.
		/// </summary>
		public User() {
			this.Created = DateTime.Now;
			this.LastLoggedIn = DateTime.Now;
		}

		/// <summary>
		/// Gets or sets the date the user was created. [Required]
		/// </summary>
		[Required]
		public DateTime Created { get; set; }

		/// <summary>
		/// Gets or sets the email.
		/// </summary>
		[MaxLength(260)]
		public string Email { get; set; }

		/// <summary>
		/// Gets or sets the email MD5 hash for gravatar.
		/// </summary>
		public Guid EmailMD5 { get; set; }

		/// <summary>
		/// Gets or sets the id. [Key]
		/// </summary>
		[Key]
		public int Id { get; set; }

		/// <summary>
		/// Gets or sets the date the user last logged in. [Required]
		/// </summary>
		[Required]
		public DateTime LastLoggedIn { get; set; }

		/// <summary>
		/// Gets or sets the user's name. [Required]
		/// </summary>
		[Required]
		[MaxLength(100)]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the token.
		/// </summary>
		[MaxLength(64)]
		public Guid Token { get; set; }

		/// <summary>
		/// Gets or sets the posting key.
		/// </summary>
		[MaxLength(64)]
		public Guid PostingKey { get; set; }

		/// <summary>
		/// Gets or sets the user's open id authentication URL. [Required]
		/// </summary>
		public virtual ICollection<ClaimedId> ClamedIdentifiers {
			get { return this.clamedIds ?? (this.clamedIds = new List<ClaimedId>()); }
			set { this.clamedIds = value; }
		}

		/// <summary>
		/// Gets or sets the friends of the user.
		/// </summary>
		public virtual ICollection<Friendship> Friends {
			get { return this.friends ?? (this.friends = new List<Friendship>()); }
			set { this.friends = value; }
		}

		/// <summary>
		/// Gets or sets the posts.
		/// </summary>
		public virtual ICollection<Post> Posts {
			get { return this.posts ?? (this.posts = new List<Post>()); }
			set { this.posts = value; }
		}
	}
}
