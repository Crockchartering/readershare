﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GibixOnline.ReaderShareProject.Models {
	using System.ComponentModel.DataAnnotations;

	public class Friendship {
		[Key]
		public int Id { get; set; }

		public int UserId { get; set; }

		public virtual User User { get; set; }
		public virtual User Friend { get; set; }
	}
}