﻿namespace GibixOnline.ReaderShareProject.Models {
	#region Using directives

	using System;
	using System.ComponentModel.DataAnnotations;
	using System.Web.Mvc;

	#endregion

	/// <summary>
	/// Represents a single post for the feed.
	/// </summary>
	public class Post {
		/// <summary>
		/// Gets or sets the content.
		/// </summary>
		[AllowHtml]
		public string Content { get; set; }

		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		[Key]
		public int Id { get; set; }

		/// <summary>
		/// Gets or sets the date the post was last updated.
		/// </summary>
		public DateTime LastUpdated { get; set; }

		/// <summary>
		/// Gets or sets the original URI.
		/// </summary>
		public Uri OriginalUri { get; set; }

		/// <summary>
		/// Gets or sets the original URI string.
		/// </summary>
		public string OriginalUriString {
			get {
				if (this.OriginalUri != null) {
					return this.OriginalUri.AbsoluteUri;
				}

				return null;
			}

			set {
				Uri uri;
				if (!String.IsNullOrWhiteSpace(value) && Uri.TryCreate(value, UriKind.RelativeOrAbsolute, out uri)) {
					this.OriginalUri = uri;
				}
			}
		}

		/// <summary>
		/// Gets or sets the owner id.
		/// </summary>
		public int OwnerId { get; set; }

		/// <summary>
		/// Gets or sets the owner.
		/// </summary>
		public virtual User Owner { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="Post"/> is public.
		/// </summary>
		public bool Public { get; set; }

		/// <summary>
		/// Gets or sets the date the post was originally published.
		/// </summary>
		public DateTime Published { get; set; }

		/// <summary>
		/// Gets or sets the summary.
		/// </summary>
		[MaxLength(500)]
		[AllowHtml]
		public string Summary { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		[MaxLength(260)]
		[Required]
		public string Title { get; set; }
	}
}
