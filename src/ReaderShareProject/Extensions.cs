﻿namespace GibixOnline.ReaderShareProject {
	#region Using directives

	using System;
	using System.Globalization;
	using System.Web.Mvc;
	using System.Web.Routing;

	using GibixOnline.ReaderShareProject.Support;

	#endregion

	/// <summary>
	/// Contains useful extension methods for use throughout the application.
	/// </summary>
	public static class Extensions {
		/// <summary>
		///   Replaces the format item in a specified string with the string representation
		///   of a corresponding object in a specified array.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An object array that contains zero or more objects to format.</param>
		/// <returns>
		///   A copy of format in which the format items have been replaced by the string
		///   representation of the corresponding objects in <paramref name="args"/>.
		/// </returns>
		public static string FormatIC(this string format, params object[] args) {
			return String.Format(CultureInfo.InvariantCulture, format, args);
		}

		/// <summary>
		/// Adds a new lowercase route mapping.
		/// </summary>
		/// <param name="routes">The routes collection.</param>
		/// <param name="name">The route name.</param>
		/// <param name="url">The URL for the route.</param>
		/// <param name="defaults">The defaults for the route parameters.</param>
		internal static void MapRouteLowercase(this RouteCollection routes, string name, string url, object defaults) { routes.MapRouteLowercase(name, url, defaults, null); }

		/// <summary>
		/// Adds a new lowercase route mapping.
		/// </summary>
		/// <param name="routes">The routes collection.</param>
		/// <param name="name">The route name.</param>
		/// <param name="url">The URL for the route.</param>
		/// <param name="defaults">The defaults for the route parameters.</param>
		/// <param name="constraints">The constraints for the route parameters.</param>
		internal static void MapRouteLowercase(this RouteCollection routes, string name, string url, object defaults, object constraints) {
			if (routes == null) {
				throw new ArgumentNullException("routes");
			}

			if (url == null) {
				throw new ArgumentNullException("url");
			}

			var route = new LowercaseRoute(url, new MvcRouteHandler())
			{
				Defaults = new RouteValueDictionary(defaults),
				Constraints = new RouteValueDictionary(constraints)
			};

			if (string.IsNullOrWhiteSpace(name)) {
				routes.Add(route);
			} else {
				routes.Add(name, route);
			}
		}
	}
}
