﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GibixOnline.ReaderShareProject.Support {
	using System.Configuration;
	using System.Data;
	using System.Data.Common;
	using System.Threading;

	internal static class Engine {
		static readonly object FactoryLock = new object();

		static string ConnectionString { get; set; }
		static DbProviderFactory Factory { get; set; }

		internal static IDbConnection GetConnection() {
			if (Factory == null) {
				lock (FactoryLock) {
					if (Factory == null) {
						string provider = ConfigurationManager.AppSettings["DatabaseProviderName"];

						if (String.IsNullOrWhiteSpace(provider)) {
							throw new ConfigurationErrorsException("Missing DatabaseProviderName");
						}

						var connectionSettings = ConfigurationManager.ConnectionStrings.OfType<ConnectionStringSettings>().SingleOrDefault(c => c.ProviderName == provider);
						if (connectionSettings == null) {
							throw new ConfigurationErrorsException("Missing Database connection information.");
						}

						DbProviderFactory factory = DbProviderFactories.GetFactory(provider);

						Thread.MemoryBarrier();
						ConnectionString = connectionSettings.ConnectionString;
						Factory = factory;
					}
				}
			}

			IDbConnection connection = Factory.CreateConnection();
			connection.ConnectionString = ConnectionString;
			return connection;
		}
	}
}