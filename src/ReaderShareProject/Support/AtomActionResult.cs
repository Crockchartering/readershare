﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GibixOnline.ReaderShareProject.Support {
	using System.ServiceModel.Syndication;
	using System.Web.Mvc;
	using System.Xml;

	public class AtomActionResult : ActionResult {
		SyndicationFeed feed;

		public AtomActionResult(SyndicationFeed feed) {
			this.feed = feed;
		}

		public override void ExecuteResult(ControllerContext context) {
			context.HttpContext.Response.ContentType = "application/atom+xml";

			Atom10FeedFormatter rssFormatter = new Atom10FeedFormatter(this.feed);
			using (XmlWriter writer = XmlWriter.Create(context.HttpContext.Response.Output)) {
				rssFormatter.WriteTo(writer);
			}
		}
	}
}